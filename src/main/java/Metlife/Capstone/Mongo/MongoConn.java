package Metlife.Capstone.Mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.Cursor;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ParallelScanOptions;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static java.util.concurrent.TimeUnit.SECONDS;

public class MongoConn {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 System.out.println("Hello, ");
		 
			 try {
				MongoClient mongoClient = new MongoClient("localhost" , 27017 );
				
				DB db = mongoClient.getDB("test");
				
				Set<String> colls = db.getCollectionNames();

				for (String s : colls) {
				    System.out.println(s);
				}
				
				
				DBCollection mtu = db.getCollection("mtu");
				
				BasicDBObject doc = new BasicDBObject("food", "Yogurt")
				.append("type", "Greek")
				.append("count", 1)
				.append("status",new BasicDBObject("meal","lunch").append("consumed", false))
				.append("hungry", true);
				
				mtu.insert(doc);
				BasicDBObject query = new BasicDBObject("food","Yogurt");
				DBCursor cursor;
				cursor = mtu.find(query);
				while (cursor.hasNext()){
					System.out.println(cursor.next());
				}
				cursor.close();
				
				 mongoClient.close();
			} catch (UnknownHostException e) {
				System.out.println("hi");
				e.printStackTrace();
			}
			
		 
	}

}
